import React from 'react';
import styles from './Card.module.css';
import {Link} from 'react-router-dom';

function Card(props){

    const { card } = props;

    return(
        <div className={styles.Card}>
            <img src={card.image} alt="image"/>
            <h4>{card.name}</h4>

        <Link to={`/card-detail/${card.id}`}>View More</Link>
        </div>
    )
}

export default Card;