import React from 'react';

function CardDetail(props){

    let {card} = props;

    return(
        <div>
            <h2>{card.name}</h2>
            <img src={card.image}/>
            <h4>Location: {card.location.name}</h4>
            <h4>Origin: {card.origin.name}</h4>
            <h4>Species: {card.species}</h4>
            <h4>Gender: {card.gender}</h4>
            <h4>Status: {card.status}</h4>
        </div>
    )
}

export default CardDetail;