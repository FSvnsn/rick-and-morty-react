import React from 'react';
import CardDetail from '../card-detail/CardDetail';

class Profile extends React.Component{

    state = {
        card: null
    }

    componentDidMount(){
        const { id } = this.props.match.params;

        fetch(`https://rickandmortyapi.com/api/character/${id}`).then(resp => resp.json())
            .then(resp => {
                this.setState({card: resp});
            });
        
            
    }

    render(){

        let cardDetailComponent = null;

        if(this.state.card != null){
            cardDetailComponent = <CardDetail card={this.state.card}/>;
        }
        else{
            cardDetailComponent = <p> Loading profile </p>;
        }

        return(
            <div>
                {cardDetailComponent}
            </div>
        )
    }
}

export default Profile;