import React from 'react';
import styles from './card-list.module.css';
import Card from '../card/Card'

class CardList extends React.Component {
    state = {
        cards: []
    }

    //Executes when the html has been loaded into the dom
    componentDidMount() {

        fetch('https://rickandmortyapi.com/api/character/').then(resp => resp.json())
            .then(resp => {
                this.setState({cards: [...resp.results]})
            }).catch(e => {
                console.log(e)
            });
    };

    render() {

        let cardComponents = null;

        if(this.state.cards.length > 0){
            //Build an array of card components
            cardComponents = this.state.cards.map(card => {
                return (
                    <Card card={card} key={card.id}/>
                )
            });
        }
        else{
            //set loading message
            cardComponents = <p>Loading Rick and Morty ...</p>;
        }

        return (
            <div className={styles.CardList}>
                {cardComponents}
            </div>
        )
    }
}

export default CardList;