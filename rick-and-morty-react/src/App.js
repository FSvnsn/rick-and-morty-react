import React from 'react';
import CardList from './components/card-list/card-list'
import {Route} from 'react-router-dom'
import Profile from './components/profile/Profile';

function App(){

    return(
        <React.Fragment>
            <Route path="/" exact component={CardList}/>
            <Route path="/card-detail/:id" component={Profile}/>
        </React.Fragment>
    );
}

export default App;